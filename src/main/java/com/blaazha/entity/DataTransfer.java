package com.blaazha.entity;

import lombok.*;
import org.nd4j.shade.jackson.annotation.JsonProperty;

@Data
@AllArgsConstructor
public class DataTransfer {

    @Setter(AccessLevel.NONE)
    private String header;

    @Setter(AccessLevel.NONE)
    @JsonProperty("data")
    private String jsonData;
}
