package com.blaazha.httpserver.controller;

import com.blaazha.httpserver.controller.exception.ActivationException;
import com.sun.net.httpserver.HttpExchange;
import java.io.IOException;

public interface Controller {
    void onStartup(HttpExchange exchange) throws IOException;
    void canActivate(HttpExchange exchange) throws ActivationException;

    void process(HttpExchange exchange) throws IOException;
}
