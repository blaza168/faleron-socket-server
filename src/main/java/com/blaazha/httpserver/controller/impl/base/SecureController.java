package com.blaazha.httpserver.controller.impl.base;

import com.blaazha.httpserver.controller.exception.ActivationException;
import com.blaazha.httpserver.service.RequestParser;
import com.blaazha.httpserver.service.RequestValidator;
import com.blaazha.service.SocketContainer;
import com.blaazha.service.SocketSender;
import com.google.inject.Inject;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;

public abstract class SecureController extends BaseController {

    protected final RequestValidator requestValidator;

    @Inject
    public SecureController(RequestValidator requestValidator, SocketContainer socketStorage, SocketSender socketSender, RequestParser requestParser) {
        super(socketStorage, socketSender, requestParser);
        this.requestValidator = requestValidator;
    }

    public SecureController(SocketContainer socketStorage, SocketSender socketSender, RequestParser requestParser, RequestValidator requestValidator) {
        super(socketStorage, socketSender, requestParser);
        this.requestValidator = requestValidator;
    }

    @Override
    public void canActivate(HttpExchange exchange) throws ActivationException {
        if (!this.requestValidator.isValid(this.requestParams)) {
            throw new ActivationException("Parameters are not valid");
        }
    }

    @Override
    public abstract void process(HttpExchange exchange) throws IOException;
}
