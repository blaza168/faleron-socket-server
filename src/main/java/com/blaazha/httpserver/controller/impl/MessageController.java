package com.blaazha.httpserver.controller.impl;

import com.blaazha.httpserver.controller.exception.ActivationException;
import com.blaazha.httpserver.controller.impl.base.SecureController;
import com.blaazha.httpserver.service.RequestParser;
import com.blaazha.httpserver.service.RequestValidator;
import com.blaazha.service.SocketContainer;
import com.blaazha.service.SocketSender;
import com.google.inject.Inject;
import com.sun.net.httpserver.HttpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

public class MessageController extends SecureController {

    private final Logger log = LoggerFactory.getLogger(MessageController.class);

    @Inject
    public MessageController(RequestValidator requestValidator, SocketContainer socketStorage, SocketSender socketSender, RequestParser requestParser) {
        super(requestValidator, socketStorage, socketSender, requestParser);
    }

    @Override
    public void canActivate(HttpExchange exchange) throws ActivationException {
        super.canActivate(exchange);
        if (!this.requestParams.containsKey("recipients")) {
            throw new ActivationException("Missing parameter: recipients");
        }
    }

    @Override
    public void process(HttpExchange exchange) throws IOException {
        String[] recipients = this.requestParams.get("recipients").split(",");

        Collection<Integer> ids = new HashSet<>();

        for (String id: recipients) {
            ids.add(Integer.getInteger(id));
        }

        this.socketSender.sendToUsers(ids, this.requestParams.get("header"), this.requestParams.get("data"));

        this.sendSuccessResponse(exchange);
    }
}
