package com.blaazha.httpserver.controller.impl.base;

import com.blaazha.httpserver.controller.Controller;
import com.blaazha.httpserver.controller.exception.ActivationException;
import com.blaazha.httpserver.service.RequestParser;
import com.blaazha.httpserver.service.RequestValidator;
import com.blaazha.service.SocketSender;
import com.blaazha.service.SocketContainer;
import com.sun.net.httpserver.HttpExchange;
import org.nd4j.shade.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Map;

public abstract class BaseController implements Controller {

    private final Logger log = LoggerFactory.getLogger(BaseController.class);

    protected final SocketContainer socketStorage;
    protected final SocketSender socketSender;
    protected final RequestParser requestParser;

    protected Map<String, String> requestParams;

    public BaseController(SocketContainer socketStorage, SocketSender socketSender, RequestParser requestParser) {
        this.socketStorage = socketStorage;
        this.socketSender = socketSender;
        this.requestParser = requestParser;
    }

    @Override
    public void onStartup(HttpExchange exchange) throws IOException {
        this.requestParams = this.requestParser.parseRequest(exchange);
    }

    @Override
    public void canActivate(HttpExchange exchange) throws ActivationException {
        // no activation rules required
    }

    @Override
    public abstract  void process(HttpExchange exchange) throws IOException;


    protected void sendJson(HttpExchange exchange, Object jsonObject) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        String json = objectMapper.writeValueAsString(jsonObject);
        exchange.getResponseHeaders().set("Content-type", "application/json");
        exchange.sendResponseHeaders(200, json.length());
        OutputStream outputStream = exchange.getResponseBody();
        outputStream.write(json.getBytes());
        outputStream.close();
    }

    protected void sendSuccessResponse(HttpExchange exchange) throws IOException {
        String response = "OK";
        exchange.sendResponseHeaders(200, response.length());
        OutputStream os = exchange.getResponseBody();
        os.write(response.getBytes());
        os.close();
    }
}
