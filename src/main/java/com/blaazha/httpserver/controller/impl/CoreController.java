package com.blaazha.httpserver.controller.impl;

import com.blaazha.httpserver.controller.impl.base.SecureController;
import com.blaazha.httpserver.service.RequestParser;
import com.blaazha.httpserver.service.RequestValidator;
import com.blaazha.service.SocketContainer;
import com.blaazha.service.SocketSender;
import com.google.inject.Inject;
import com.sun.net.httpserver.HttpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class CoreController extends SecureController {

    private final Logger log = LoggerFactory.getLogger(CoreController.class);

    @Inject
    public CoreController(RequestValidator requestValidator, SocketContainer socketStorage, SocketSender socketSender, RequestParser requestParser) {
        super(requestValidator, socketStorage, socketSender, requestParser);
    }

    @Override
    public void process(HttpExchange exchange) throws IOException {
        log.info("Core request");

        this.socketSender.sendToRoom(this.requestParams.get("room"), this.requestParams.get("header"), this.requestParams.get("data"));

        this.sendJson(exchange, "OK");
    }
}
