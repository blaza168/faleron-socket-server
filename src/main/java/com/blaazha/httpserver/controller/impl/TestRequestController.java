package com.blaazha.httpserver.controller.impl;

import com.blaazha.httpserver.controller.exception.ActivationException;
import com.blaazha.httpserver.controller.impl.base.BaseController;
import com.blaazha.httpserver.service.RequestParser;
import com.blaazha.httpserver.service.RequestValidator;
import com.blaazha.service.SocketSender;
import com.blaazha.service.SocketContainer;
import com.google.inject.Inject;
import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.io.OutputStream;

public class TestRequestController extends BaseController {

    @Inject
    public TestRequestController(SocketContainer socketStorage, SocketSender socketSender, RequestParser requestParser) {
        super(socketStorage, socketSender, requestParser);
    }

    @Override
    public void process(HttpExchange exchange) throws IOException {
        this.sendSuccessResponse(exchange);
    }
}
