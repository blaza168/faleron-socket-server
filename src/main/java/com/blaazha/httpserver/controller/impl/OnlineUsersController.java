package com.blaazha.httpserver.controller.impl;

import com.blaazha.httpserver.controller.exception.ActivationException;
import com.blaazha.httpserver.controller.impl.base.BaseController;
import com.blaazha.httpserver.service.RequestParser;
import com.blaazha.httpserver.service.RequestValidator;
import com.blaazha.service.SocketSender;
import com.blaazha.service.SocketContainer;
import com.blaazha.websocket.entity.ConnectionDetails;
import com.blaazha.websocket.entity.UserDefinition;
import com.google.inject.Inject;
import com.sun.net.httpserver.HttpExchange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class OnlineUsersController extends BaseController {

    private final Logger log = LoggerFactory.getLogger(OnlineUsersController.class);

    @Inject
    public OnlineUsersController(SocketContainer socketStorage, SocketSender socketSender, RequestParser requestParser) {
        super(socketStorage, socketSender, requestParser);
    }

    @Override
    public void process(HttpExchange exchange) throws IOException {
        log.info("online users request");
        this.requestParams = this.requestParser.parseRequest(exchange);
        this.setAccessControlHeader(exchange);
        if (!this.requestParams.containsKey("room")) {
            this.sendJson(exchange, getUsers());
        } else {
            this.sendJson(exchange, getUsersInRoom(this.requestParams.get("room")));
        }
    }

    private void setAccessControlHeader(HttpExchange exchange) {
        exchange.getResponseHeaders().set("Access-Control-Allow-Origin", "*");
    }

    private List<UserDefinition> getUsersInRoom(String room) {
        List<UserDefinition> connectedUsers = new ArrayList<>();

        Set<ConnectionDetails> userDefinitionSet = this.socketStorage.getConnections().getOrDefault(room, new HashSet<>());

        for (ConnectionDetails connection: userDefinitionSet) {
            connectedUsers.add(connection.getUserDefinition());
        }

        return connectedUsers;
    }

    private List<UserDefinition> getUsers() {
        List<UserDefinition> connectedUsers = new ArrayList<>();

        for (Map.Entry<String, Set<ConnectionDetails>> room:
             socketStorage.getConnections().entrySet()) {
            for (ConnectionDetails client: room.getValue()) {
                connectedUsers.add(client.getUserDefinition());
            }
        }

        return connectedUsers;
    }
}
