package com.blaazha.httpserver.controller.exception;

import java.io.IOException;

public class ActivationException extends IOException {
    public ActivationException(String message) {
        super(message);
    }
}
