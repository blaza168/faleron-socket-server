package com.blaazha.httpserver.service.impl;

import com.blaazha.httpserver.service.RequestParser;
import com.sun.net.httpserver.HttpExchange;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class RequestParserImpl implements RequestParser {

    @Override
    public Map<String, String> parseRequest(HttpExchange exchange) throws IOException {
        if (exchange.getRequestMethod().equals("GET")) {
            return this.mapQuery(exchange.getRequestURI().getQuery());
        } else {
            return this.getPostParams(exchange);
        }
    }

    private Map<String, String> mapQuery(String query) throws IOException {
        Map<String, String> result = new HashMap<>();
        if (query == null) {
            return result;
        }
        for (String param : query.split("&")) {
            String[] entry = param.split("=");
            if (entry.length > 1) {
                result.put(URLDecoder.decode(entry[0], "UTF-8"), URLDecoder.decode(entry[1], "UTF-8"));
            } else {
                result.put(entry[0], "");
            }
        }
        return result;
    }


    private Map<String, String> getPostParams(HttpExchange httpExchange) throws IOException {
        // https://gist.github.com/williamsandytoes/82437e08c4bdb1bc37130a259ee96822
        Map<String, String> parameters = new HashMap<>();
        InputStream inputStream = httpExchange.getRequestBody();
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[2048];
        int read = 0;
        while ((read = inputStream.read(buffer)) != -1) {
            byteArrayOutputStream.write(buffer, 0, read);
        }
        String[] keyValuePairs = byteArrayOutputStream.toString().split("&");
        for (String keyValuePair : keyValuePairs) {
            String[] keyValue = keyValuePair.split("=");
            if (keyValue.length != 2) {
                continue;
            }
            parameters.put(URLDecoder.decode(keyValue[0], "UTF-8"), URLDecoder.decode(keyValue[1], "UTF-8"));
        }
        return parameters;
    }
}
