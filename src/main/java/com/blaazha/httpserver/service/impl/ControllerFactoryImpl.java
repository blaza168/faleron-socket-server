package com.blaazha.httpserver.service.impl;

import com.blaazha.httpserver.controller.Controller;
import com.blaazha.httpserver.controller.impl.CoreController;
import com.blaazha.httpserver.controller.impl.MessageController;
import com.blaazha.httpserver.controller.impl.OnlineUsersController;
import com.blaazha.httpserver.controller.impl.TestRequestController;
import com.blaazha.httpserver.router.Router;
import com.blaazha.httpserver.service.ControllerFactory;
import com.google.inject.Inject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ControllerFactoryImpl implements ControllerFactory {

    private final Router router;

    private final TestRequestController testRequestController;
    private final OnlineUsersController onlineUsersController;
    private final CoreController coreController;
    private final MessageController messageController;


    @Inject
    public ControllerFactoryImpl(TestRequestController testRequestHandler,
                                 OnlineUsersController onlineUsersController,
                                 CoreController coreController,
                                 MessageController messageController,
                                 Router router) {
        this.testRequestController = testRequestHandler;
        this.onlineUsersController = onlineUsersController;
        this.coreController = coreController;
        this.messageController = messageController;

        this.router = router;
    }

    private Set<Controller> getControllers() {
        Set<Controller> controllers = new HashSet<>();

        controllers.add(this.testRequestController);
        controllers.add(this.onlineUsersController);
        controllers.add(this.coreController);
        controllers.add(this.messageController);

        return controllers;
    }

    @Override
    public Controller getController(String path) {
        // avoid null pointer exception in router class
        if (path == null) {
            return null;
        }

        Class<? extends Controller> controllerClass = this.router.getControllerForPath(path);

        for (Controller controller: getControllers()) {
            if (controller.getClass() == controllerClass) {
                return controller;
            }
        }

        return null;
    }
}
