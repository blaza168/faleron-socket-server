package com.blaazha.httpserver.service.impl;

import com.blaazha.httpserver.service.RequestValidator;
import org.aeonbits.owner.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class RequestValidatorImpl implements RequestValidator {

    private String passwordKey;
    private String passwordValue;

    public RequestValidatorImpl(String passwordKey, String passwordValue) {
        this.passwordKey = passwordKey;
        this.passwordValue = passwordValue;
    }

    @Override
    public boolean isValid(Map<String, String> parameters) {

        if (!passwordValue.equals(parameters.get(passwordKey))) {
            return false;
        }

        for (String parameter: new String[] {"room", "data", "header"}) {
            if (!parameters.containsKey(parameter)) {
                return false;
            }
        }


        return true;
    }
}
