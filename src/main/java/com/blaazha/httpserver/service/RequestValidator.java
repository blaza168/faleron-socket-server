package com.blaazha.httpserver.service;

import java.util.Map;

public interface RequestValidator {
    boolean isValid(Map<String, String> parameters);
}
