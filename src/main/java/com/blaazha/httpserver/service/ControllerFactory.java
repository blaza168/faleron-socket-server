package com.blaazha.httpserver.service;


import com.blaazha.httpserver.controller.Controller;
import com.sun.net.httpserver.HttpHandler;

import java.util.Map;

public interface ControllerFactory {
    Controller getController(String path);
}
