package com.blaazha.httpserver.service;

import com.sun.net.httpserver.HttpExchange;

import java.io.IOException;
import java.util.Map;

public interface RequestParser {
    Map<String, String> parseRequest(HttpExchange exchange) throws IOException;
}
