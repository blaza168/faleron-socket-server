package com.blaazha.httpserver.handler;


import com.blaazha.httpserver.controller.Controller;
import com.blaazha.httpserver.service.ControllerFactory;
import com.google.inject.Inject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;
import java.io.OutputStream;


public class ApplicationRequestHandler implements HttpHandler {

    private final Logger log = LoggerFactory.getLogger(ApplicationRequestHandler.class);

    private final ControllerFactory controllerFactory;

    private Controller controller;

    @Inject
    public ApplicationRequestHandler(ControllerFactory controllerFactory) {
        this.controllerFactory = controllerFactory;
    }

    @Override
    public void handle(HttpExchange exchange) throws IOException {
        log.debug("new request");
        controller = controllerFactory.getController(exchange.getRequestURI().getPath());

        if (controller == null) {
            notFound(exchange);
            log.debug("controller not found");
            return;
        }

        try {
            controller.onStartup(exchange);
            controller.canActivate(exchange);
            controller.process(exchange);
        } catch (Exception e) {
            if (e instanceof IOException) {
                badRequest(exchange, e);
            } else {
                e.printStackTrace();
                serverError(exchange);
            }
        }
    }

    private void badRequest(HttpExchange exchange, Exception e) throws IOException {
        String message = e.getMessage();
        exchange.sendResponseHeaders(500, message.length());
        OutputStream os = exchange.getResponseBody();
        os.write(message.getBytes());
        os.close();
    }

    private void serverError(HttpExchange exchange) throws IOException {
        String message = "Server error";
        exchange.sendResponseHeaders(500, message.length());
        OutputStream os = exchange.getResponseBody();
        os.write(message.getBytes());
        os.close();
    }

    private void notFound(HttpExchange exchange) throws IOException {
        String message = "Not found";
        exchange.sendResponseHeaders(404, message.length());
        OutputStream os = exchange.getResponseBody();
        os.write(message.getBytes());
        os.close();
    }

}
