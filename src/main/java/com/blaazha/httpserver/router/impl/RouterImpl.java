package com.blaazha.httpserver.router.impl;

import com.blaazha.httpserver.controller.Controller;
import com.blaazha.httpserver.controller.impl.CoreController;
import com.blaazha.httpserver.controller.impl.MessageController;
import com.blaazha.httpserver.controller.impl.OnlineUsersController;
import com.blaazha.httpserver.controller.impl.TestRequestController;
import com.blaazha.httpserver.router.Router;
import java.util.HashMap;
import java.util.Map;

public class RouterImpl implements Router {

    private Map<String, Class<? extends Controller>> configureMapping() {

        // path -> class
        Map<String, Class<? extends Controller>> mapping = new HashMap<>();

        mapping.put("/", CoreController.class);
        mapping.put("/online-users", OnlineUsersController.class);
        mapping.put("/test", TestRequestController.class);
        mapping.put("/post", MessageController.class);

        return mapping;
    }

    @Override
    public Class<? extends Controller> getControllerForPath(String path) {
        Map<String, Class<? extends Controller>> mapping = configureMapping();
        return mapping.getOrDefault(path, null);
    }
}
