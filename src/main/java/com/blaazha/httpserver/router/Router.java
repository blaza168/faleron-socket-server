package com.blaazha.httpserver.router;

import com.blaazha.httpserver.controller.Controller;

public interface Router {
    Class<? extends Controller> getControllerForPath(String path);
}
