package com.blaazha.httpserver;

import com.google.inject.Inject;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.io.IOException;

public class MyHttpServer {

    private static Logger log = LoggerFactory.getLogger(MyHttpServer.class);

    private final HttpServer httpServer;
    private final HttpHandler httpHandler;

    @Inject
    public MyHttpServer(HttpServer httpServer, HttpHandler httpHandler) {
        this.httpServer = httpServer;
        this.httpHandler = httpHandler;
    }

    public void run() throws IOException {
        httpServer.createContext("/", httpHandler);
        httpServer.setExecutor(null); // creates a default executor
        httpServer.start();
        log.info("http is running on port " + httpServer.getAddress().getPort());
    }


    public void stop() {
        this.httpServer.stop(1000);
    }
}
