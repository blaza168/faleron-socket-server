package com.blaazha.httpserver;

import com.google.common.util.concurrent.AbstractIdleService;
import com.google.inject.Inject;

public class HttpServerApplicationService extends AbstractIdleService {

    private MyHttpServer httpServer;

    @Inject
    public HttpServerApplicationService(MyHttpServer httpServer) {
        this.httpServer = httpServer;
    }

    @Override
    protected void startUp() throws Exception {
        this.httpServer.run();
    }

    @Override
    protected void shutDown() throws Exception {
        this.httpServer.stop();
    }
}
