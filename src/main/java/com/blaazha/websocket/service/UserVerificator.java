package com.blaazha.websocket.service;

import com.blaazha.websocket.entity.ConnectionDetails;

public interface UserVerificator {

    boolean verificate(ConnectionDetails connectionDetails);

}
