package com.blaazha.websocket.service;

import com.blaazha.websocket.entity.ConnectionDetails;

public interface WebSocketService {
    ConnectionDetails getConnectionDetails(String resourceDescription);

    boolean verify(ConnectionDetails connectionDetails);
}
