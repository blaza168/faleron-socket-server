package com.blaazha.websocket.service;

import com.blaazha.websocket.entity.ConnectionDetails;

public interface QueryParametersParser {
    ConnectionDetails parse(String query);
}
