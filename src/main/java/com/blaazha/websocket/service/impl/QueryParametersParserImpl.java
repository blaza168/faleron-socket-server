package com.blaazha.websocket.service.impl;

import com.blaazha.websocket.entity.ConnectionDetails;
import com.blaazha.websocket.entity.UserDefinition;
import com.blaazha.websocket.service.QueryParametersParser;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;

public class QueryParametersParserImpl implements QueryParametersParser {
    @Override
    public ConnectionDetails parse(String query) {
        //   /?name=blem

        int startingQueryIndex = query.indexOf('?');
        String endpoint = query.substring(1, startingQueryIndex);
        Map<String, String> params = parseQueryParameters(query.substring(startingQueryIndex + 1));

        if (params.getOrDefault("id", null) == null) { // user is not logged in
            return this.getAnonymousDetails(params, endpoint);
        }

        try {
            UserDefinition userDefinition = new UserDefinition(Integer.parseInt(params.get("id")), params.get("name"), params.get("route"), params.get("image"));
            ConnectionDetails connectionDetails = new ConnectionDetails(params.get("room"), endpoint, userDefinition, params.get("key"));
            return connectionDetails;
        } catch (NumberFormatException e) {
            return null;
        }
    }

    private ConnectionDetails getAnonymousDetails(Map<String, String> params, String endpoint) {
        UserDefinition userDefinition = new UserDefinition();
        ConnectionDetails connectionDetails = new ConnectionDetails(params.getOrDefault("room", "homepage:default"), endpoint, userDefinition, null);
        return connectionDetails;
    }

    private Map<String, String> parseQueryParameters(String query) {
        Map<String, String> params = new HashMap<>();

        // maybe this can be solved by streams, but I don´t know how :-)  (https://stackoverflow.com/questions/13592236/parse-a-uri-string-into-name-value-collection)
        for (String input: query.split("&")) {
            String[] splittedInput = input.split("=");
            if (splittedInput.length == 2) {
                try {
                    params.put(URLDecoder.decode(splittedInput[0], "UTF-8"), URLDecoder.decode(splittedInput[1], "UTF-8"));
                } catch (UnsupportedEncodingException e) {
                }
            }
        }

        return params;
    }
}
