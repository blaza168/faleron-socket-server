package com.blaazha.websocket.service.impl;

import com.blaazha.websocket.entity.ConnectionDetails;
import com.blaazha.websocket.entity.UserDefinition;
import com.blaazha.websocket.entity.db.User;
import com.blaazha.websocket.repository.UserRepository;
import com.blaazha.websocket.service.UserVerificator;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.soap.SOAPBinding;

public class UserVerificatorImpl implements UserVerificator {

    private final Logger log = LoggerFactory.getLogger(UserVerificator.class);

    private final UserRepository userRepository;

    @Inject
    public UserVerificatorImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public boolean verificate(ConnectionDetails connectionDetails) {
        if (connectionDetails.getUserDefinition().isAnonymous()) {
            return true;
        }
        User user = userRepository.getUser(connectionDetails.getUserDefinition().getName());
        if (user != null) {
            return validateUser(user, connectionDetails);
        }
        return false;
    }

    /**
     * @param user user from DB
     * @param connectionDetails informations from user
     * @return result of validation
     */
    private boolean validateUser(User user, ConnectionDetails connectionDetails) {
        UserDefinition u = connectionDetails.getUserDefinition();

        if (!user.getName().equals(u.getName())) {
            return false;
        }

        if (!user.getSocketKey().equals(connectionDetails.getSocketKey())) {
            return false;
        }

        return true;
    }
}
