package com.blaazha.websocket.service.impl;

import com.blaazha.websocket.entity.ConnectionDetails;
import com.blaazha.websocket.service.QueryParametersParser;
import com.blaazha.websocket.service.UserVerificator;
import com.blaazha.websocket.service.WebSocketService;
import com.google.inject.Inject;

public class WebSocketServiceImpl implements WebSocketService {

    private final QueryParametersParser queryParametersParser;
    private final UserVerificator userVerificator;

    @Inject
    public WebSocketServiceImpl(QueryParametersParser queryParametersParser, UserVerificator userVerificator) {
        this.queryParametersParser = queryParametersParser;
        this.userVerificator = userVerificator;
    }

    @Override
    public ConnectionDetails getConnectionDetails(String resourceDescription) {
        return this.queryParametersParser.parse(resourceDescription);
    }

    @Override
    public boolean verify(ConnectionDetails connectionDetails) {
        return this.userVerificator.verificate(connectionDetails);
    }
}
