package com.blaazha.websocket;

import com.blaazha.service.SocketContainer;
import com.blaazha.websocket.entity.ConnectionDetails;
import com.blaazha.websocket.service.WebSocketService;
import com.google.inject.Inject;
import com.google.inject.name.Named;
import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetSocketAddress;
import java.util.HashSet;
import java.util.Set;

public class MyWebSocketServer extends WebSocketServer {

    private final Logger log = LoggerFactory.getLogger(MyWebSocketServer.class);

    private WebSocketService webSocketService;
    private SocketContainer socketContainer;

    @Inject
    public MyWebSocketServer(WebSocketService webSocketService,
                             SocketContainer socketStorage,
                             @Named("socket-port") int socketPort) {
        super(new InetSocketAddress(socketPort));
        this.webSocketService = webSocketService;
        this.socketContainer = socketStorage;
        log.info("Socket server is running on port " + socketPort);
    }

    @Override
    public void onOpen(WebSocket conn, ClientHandshake handshake) {
        System.out.println("New connection from " + conn.getRemoteSocketAddress().getAddress().getHostAddress());
        ConnectionDetails connectionDetails = webSocketService.getConnectionDetails(conn.getResourceDescriptor());
        if (connectionDetails == null) {
            return;
        }
        connectionDetails.setWebSocket(conn);
        if (!this.webSocketService.verify(connectionDetails)) {
            conn.close(400, "Verification failed");
            System.out.println("connection closed");
            return;
        }

        this.addConnectionToList(connectionDetails);
    }

    @Override
    public void onClose(WebSocket conn, int code, String reason, boolean remote) {
        this.disconnect(conn);
    }

    @Override
    public void onMessage(WebSocket conn, String message) {
        // accept messages only from PHP
    }

    @Override
    public void onError(WebSocket conn, Exception ex) {
        this.disconnect(conn);
    }

    @Override
    public void onStart() {

    }

    private void disconnect(WebSocket conn) {
        if (conn != null) {
            this.removeConnectionFromList(conn);
        } else {
            this.filterActiveConnections();
        }
        // TODO: send to all client information about disconnect
    }


    private void addConnectionToList(ConnectionDetails connectionDetails) {
        if (!this.socketContainer.getConnections().containsKey(connectionDetails.getRoom())) {
            this.socketContainer.getConnections().put(connectionDetails.getRoom(), new HashSet<>());
        }

        this.socketContainer.getConnections().get(connectionDetails.getRoom()).add(connectionDetails);
    }

    private void removeConnectionFromList(WebSocket webSocket) {
        for (Set<ConnectionDetails> setConnections : socketContainer.getConnections().values()) {
            for (ConnectionDetails conn : setConnections) {
                if (conn.getWebSocket().equals(webSocket)) {
                    setConnections.remove(conn);
                }
            }
        }
    }

    private void filterActiveConnections() {
        for (Set<ConnectionDetails> setConnections : socketContainer.getConnections().values()) {
            for (ConnectionDetails conn : setConnections) {
                if (conn.getWebSocket() == null) {
                    setConnections.remove(conn);
                }
            }
        }
    }
}
