package com.blaazha.websocket.repository;

import com.blaazha.websocket.entity.db.User;

public interface UserRepository {
    User getUser(String name);
}
