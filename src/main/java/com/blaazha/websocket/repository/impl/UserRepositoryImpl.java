package com.blaazha.websocket.repository.impl;

import com.blaazha.websocket.entity.db.User;
import com.blaazha.websocket.repository.UserRepository;
import com.google.inject.Inject;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class UserRepositoryImpl implements UserRepository {

    private final Logger log = LoggerFactory.getLogger(UserRepositoryImpl.class);

    private final DBI dbi;

    @Inject
    public UserRepositoryImpl(DBI dbi) {
        this.dbi = dbi;
    }

    @Override
    public User getUser(String name) {
        try (Handle h = dbi.open()) {

            User user = h.createQuery("SELECT id, name, role, socket_key, route FROM user WHERE name = ? LIMIT 1")
                    .bind(0, name)
                    .map((i, rs, ctx) -> {
                        User u = new User(
                                rs.getInt("id"),
                                rs.getString("name"),
                                rs.getString("role"),
                                rs.getString("socket_key"),
                                rs.getString("route")
                        );

                        return u;
                    }).first();

             return user;
        } catch (Exception e) {
            log.error("unable to retrieve values", e);
            throw e;
        }
    }
}
