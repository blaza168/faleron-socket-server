package com.blaazha.websocket.entity.db;

import lombok.*;

@Data
@AllArgsConstructor
public class User {
    @Setter(AccessLevel.NONE)
    private Integer id;
    @Setter(AccessLevel.NONE)
    private String name;
    @Setter(AccessLevel.NONE)
    private String role;
    @Setter(AccessLevel.NONE)
    private String socketKey;
    @Setter(AccessLevel.NONE)
    private String route;

}
