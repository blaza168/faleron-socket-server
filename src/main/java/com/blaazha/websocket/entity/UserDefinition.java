package com.blaazha.websocket.entity;

import lombok.*;
import org.nd4j.shade.jackson.annotation.JsonIgnore;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDefinition {
    @Setter(AccessLevel.NONE)
    private int id;
    @Setter(AccessLevel.NONE)
    private String name;
    @Setter(AccessLevel.NONE)
    private String route;
    @Setter(AccessLevel.NONE)
    private String image;

    /**
     * @return false is user is not logged in
     */
    public boolean isAnonymous() {
        return this.name == null;
    }
}
