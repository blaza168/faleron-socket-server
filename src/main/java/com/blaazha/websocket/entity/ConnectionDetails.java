package com.blaazha.websocket.entity;

import lombok.Data;
import lombok.Getter;
import org.java_websocket.WebSocket;

@Data
public class ConnectionDetails {
    @Getter
    private String room;
    @Getter
    private String endpoint;
    @Getter
    private UserDefinition userDefinition;
    @Getter
    private String socketKey;

    private WebSocket webSocket;

    public ConnectionDetails(String room, String endpoint, UserDefinition userDefinition, String socketKey) {
        this.room = room;
        this.endpoint = endpoint;
        this.userDefinition = userDefinition;
        this.socketKey = socketKey;
    }
}
