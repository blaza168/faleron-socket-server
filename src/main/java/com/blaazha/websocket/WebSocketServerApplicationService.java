package com.blaazha.websocket;

import com.google.common.util.concurrent.AbstractIdleService;
import com.google.inject.Inject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WebSocketServerApplicationService extends AbstractIdleService {

    private static final Logger log = LoggerFactory.getLogger(WebSocketServerApplicationService.class);

    private final MyWebSocketServer webSocketServer;

    @Inject
    public WebSocketServerApplicationService(MyWebSocketServer webSocketServer) {
        this.webSocketServer = webSocketServer;
    }

    @Override
    protected void startUp() throws Exception {
        log.info("starting websocket server");
        webSocketServer.start();
    }

    @Override
    protected void shutDown() throws Exception {
        log.info("shutting down websocket server");
        webSocketServer.stop();
    }
}
