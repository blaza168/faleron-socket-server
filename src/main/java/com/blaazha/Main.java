package com.blaazha;


import com.blaazha.config.CoreModule;
import com.blaazha.config.DataModule;
import com.blaazha.config.HttpServerModule;
import com.blaazha.config.WebSocketModule;
import com.blaazha.service.ConnectionTester;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static final Logger log = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) throws Exception {
        log.info("Starting socket server");

        final Injector injector = Guice.createInjector(new CoreModule(), new DataModule(),
                new WebSocketModule(), new HttpServerModule());

        injector.getInstance(ConnectionTester.class).testConnection();

        CoreModule.BoundServices services = injector.getInstance(CoreModule.BoundServices.class);

        services.start();
        log.info("All services have been successfully started");

        services.awaitStopped();
        log.info("Terminating socket server");
    }
}
