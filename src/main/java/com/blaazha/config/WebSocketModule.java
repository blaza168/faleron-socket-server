package com.blaazha.config;

import com.blaazha.httpserver.service.RequestParser;
import com.blaazha.httpserver.service.impl.RequestParserImpl;
import com.blaazha.websocket.repository.UserRepository;
import com.blaazha.websocket.repository.impl.UserRepositoryImpl;
import com.blaazha.websocket.service.QueryParametersParser;
import com.blaazha.websocket.service.UserVerificator;
import com.blaazha.websocket.service.WebSocketService;
import com.blaazha.websocket.service.impl.QueryParametersParserImpl;
import com.blaazha.websocket.service.impl.UserVerificatorImpl;
import com.blaazha.websocket.service.impl.WebSocketServiceImpl;
import com.google.inject.AbstractModule;

public class WebSocketModule extends AbstractModule {
    @Override
    protected void configure() {

        // configure interfaces
        bind(QueryParametersParser.class).to(QueryParametersParserImpl.class);
        bind(WebSocketService.class).to(WebSocketServiceImpl.class);
        bind(UserVerificator.class).to(UserVerificatorImpl.class);
        bind(RequestParser.class).to(RequestParserImpl.class);

        // repositories
        bind(UserRepository.class).to(UserRepositoryImpl.class);
    }
}
