package com.blaazha.config.vars;

import org.aeonbits.owner.Config;
import org.aeonbits.owner.Config.Sources;

@Sources({"classpath:config/application.properties"})
public interface ApplicationConfig extends Config {

    @Key("http.port")
    @DefaultValue("3000")
    int httpPort();

    @Key("http.password.value")
    String httpPasswordValue();

    @Key("http.password.key")
    String httpPasswordKey();

    @Key("socket.port")
    @DefaultValue("5000")
    int socketPort();
}
