package com.blaazha.config;

import com.blaazha.config.vars.ApplicationConfig;
import com.blaazha.httpserver.HttpServerApplicationService;
import com.blaazha.httpserver.MyHttpServer;
import com.blaazha.websocket.WebSocketServerApplicationService;
import com.google.common.collect.ImmutableList;
import com.google.common.util.concurrent.Service;
import com.google.common.util.concurrent.ServiceManager;
import com.google.inject.AbstractModule;
import com.google.inject.Inject;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.List;
import java.util.Set;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;
import com.sun.net.httpserver.HttpServer;
import org.aeonbits.owner.ConfigFactory;

public class CoreModule extends AbstractModule {

    private static final List<Class<? extends Service>> SERVICE_CLASSES =
            ImmutableList.of(WebSocketServerApplicationService.class, HttpServerApplicationService.class);

    public static class BoundServices {
        private final ServiceManager serviceManager;

        @Inject
        public BoundServices(Set<Service> services) {
            serviceManager = new ServiceManager(services);
        }

        public void start() {
            serviceManager.startAsync();
            serviceManager.awaitHealthy();
        }

        public void awaitStopped() {
            serviceManager.awaitStopped();
        }
    }


    @Override
    protected void configure() {
        // setup servers
        ApplicationConfig applicationConfig = ConfigFactory.create(ApplicationConfig.class);
        HttpServer server = buildHttpServer(applicationConfig);
        bind(HttpServer.class).toInstance(server);
        bindConstant().annotatedWith(Names.named("socket-port")).to(applicationConfig.socketPort());

        // setup BoundServices
        bind(BoundServices.class).asEagerSingleton();
        Multibinder<Service> servicesBinder = Multibinder.newSetBinder(binder(), Service.class);
        for (Class<? extends Service> serviceClass : SERVICE_CLASSES) {
            bind(serviceClass).asEagerSingleton();
            servicesBinder.addBinding().toProvider(binder().getProvider(serviceClass));
        }
    }

    private HttpServer buildHttpServer(ApplicationConfig applicationConfig) {
        try {
            return HttpServer.create(new InetSocketAddress(applicationConfig.httpPort()), 0);
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException();
        }
    }
}
