package com.blaazha.config;

import com.blaazha.config.vars.PersistenceConfig;
import com.google.inject.AbstractModule;

import javax.sql.DataSource;

import com.mysql.jdbc.Driver;
import org.aeonbits.owner.ConfigFactory;
import org.apache.commons.dbcp2.BasicDataSource;
import org.skife.jdbi.v2.DBI;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class DataModule extends AbstractModule {

    private final Logger log = LoggerFactory.getLogger(DataModule.class);

    @Override
    protected void configure() {
        PersistenceConfig dbConfig = ConfigFactory.create(PersistenceConfig.class);
        bind(PersistenceConfig.class).toInstance(dbConfig);

        DataSource dataSource = buildDataSource(dbConfig);
        bind(DataSource.class).toInstance(dataSource);

        DBI bdi = new DBI(dataSource);
        bind(DBI.class).toInstance(bdi);
    }

    private DataSource buildDataSource(PersistenceConfig config) {
        log.info("Creating connection pool to {} as user {}", config.url(), config.username());

        BasicDataSource ds = new BasicDataSource();

        ds.setDriverClassLoader(Driver.class.getClassLoader());
        ds.setUrl(config.url());
        ds.setUsername(config.username());
        ds.setPassword(config.password());

        ds.setTestOnBorrow(true);
        ds.setTestWhileIdle(true);
        ds.setNumTestsPerEvictionRun(config.poolSize());
        ds.setTimeBetweenEvictionRunsMillis(1000);
        ds.setMinEvictableIdleTimeMillis(config.poolExpiration());
        ds.setMaxConnLifetimeMillis(3000000);
        ds.setMinIdle(0);
        ds.setMaxIdle(config.poolSize());
        ds.setMaxTotal(config.poolSize());
        ds.setLogExpiredConnections(false);

        return ds;
    }
}
