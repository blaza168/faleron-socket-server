package com.blaazha.config;

import com.blaazha.config.vars.ApplicationConfig;
import com.blaazha.httpserver.handler.ApplicationRequestHandler;
import com.blaazha.httpserver.router.Router;
import com.blaazha.httpserver.router.impl.RouterImpl;
import com.blaazha.httpserver.service.ControllerFactory;
import com.blaazha.httpserver.service.RequestValidator;
import com.blaazha.service.SocketSender;
import com.blaazha.service.impl.SocketSenderImpl;
import com.blaazha.httpserver.service.impl.ControllerFactoryImpl;
import com.blaazha.httpserver.service.impl.RequestValidatorImpl;
import com.google.inject.AbstractModule;
import com.sun.net.httpserver.HttpHandler;
import org.aeonbits.owner.ConfigFactory;


public class HttpServerModule extends AbstractModule {
    @Override
    protected void configure() {
        ApplicationConfig applicationConfig = ConfigFactory.create(ApplicationConfig.class);
        bind(RequestValidator.class).toInstance(new RequestValidatorImpl(applicationConfig.httpPasswordKey(),
                applicationConfig.httpPasswordValue()));

        bind(Router.class).to(RouterImpl.class);
        bind(HttpHandler.class).to(ApplicationRequestHandler.class);
        bind(ControllerFactory.class).to(ControllerFactoryImpl.class);
        bind(SocketSender.class).to(SocketSenderImpl.class);
    }

}
