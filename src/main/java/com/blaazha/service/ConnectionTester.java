package com.blaazha.service;

import com.google.inject.Inject;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConnectionTester {

    private static final Logger log = LoggerFactory.getLogger(ConnectionTester.class);

    private final DBI dbi;

    @Inject
    public ConnectionTester(DBI dbi) {
        this.dbi = dbi;
    }

    public void testConnection() throws Exception {
        try (Handle h = dbi.open()) {
            h.select("SELECT * FROM user");
            log.info("\u001B[32mDatabase check: OK\u001B[0m");
        } catch (Exception e) {
            log.error("Database is not in good state", e);
            throw e;
        }
    }

}
