package com.blaazha.service.impl;

import com.blaazha.entity.DataTransfer;
import com.blaazha.service.SocketContainer;
import com.blaazha.service.SocketSender;
import com.blaazha.websocket.entity.ConnectionDetails;
import com.google.inject.Inject;
import org.nd4j.shade.jackson.core.JsonProcessingException;
import org.nd4j.shade.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;

public class SocketSenderImpl implements SocketSender {

    private final Logger log = LoggerFactory.getLogger(SocketSenderImpl.class);

    private final SocketContainer socketContainer;
    private final ObjectMapper objectMapper;

    @Inject
    public SocketSenderImpl(SocketContainer socketContainer) {
        this.socketContainer = socketContainer;
        objectMapper = new ObjectMapper();
    }

    @Override
    public void sendToRoom(String room, String header, String jsonData) {
        log.info("sending data to room " + room);
        Set<ConnectionDetails> connections = this.socketContainer.getConnections().getOrDefault(room, new HashSet<>());

        String jsonString = null;

        try {
            jsonString = this.getJsonString(header, jsonData);
        } catch (JsonProcessingException e) {
            return;
        }

        for (ConnectionDetails detail: connections) {
            detail.getWebSocket().send(jsonString);
        }
    }

    @Override
    public void sendToAll(String header, String jsonData) {
        String jsonString = null;

        try {
            jsonString = this.getJsonString(header, jsonData);
        } catch (JsonProcessingException e) {
            return;
        }

        for (Map.Entry<String, Set<ConnectionDetails>> entrySet: this.socketContainer.getConnections().entrySet()) {
            for (ConnectionDetails connection: entrySet.getValue()) {
                connection.getWebSocket().send(jsonString);
            }
        }
    }

    @Override
    public void sendToUser(int id, String header, String jsonData) {
        String jsonString = null;

        try {
            jsonString = this.getJsonString(header, jsonData);
        } catch (JsonProcessingException e) {
            return;
        }

        for (Map.Entry<String, Set<ConnectionDetails>> entrySet: this.socketContainer.getConnections().entrySet()) {
            for (ConnectionDetails connection: entrySet.getValue()) {
                if (connection.getUserDefinition().getId() == id) {
                    connection.getWebSocket().send(jsonString);
                    break;
                }
            }
        }
    }

    @Override
    public void sendToUsers(Collection ids, String header, String jsonData) {
        String jsonString = null;

        try {
            jsonString = this.getJsonString(header, jsonData);
        } catch (JsonProcessingException e) {
            return;
        }

        for (Map.Entry<String, Set<ConnectionDetails>> entrySet: this.socketContainer.getConnections().entrySet()) {
            for (ConnectionDetails connection: entrySet.getValue()) {
                if (ids.contains(connection.getUserDefinition().getId())) {
                    connection.getWebSocket().send(jsonString);
                    break;
                }
            }
        }
    }


    private String getJsonString(String header, String jsonData) throws JsonProcessingException {
        DataTransfer dataTransfer = new DataTransfer(header, jsonData);
        try {
            return objectMapper.writeValueAsString(dataTransfer);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            log.error("Cannot process json", e);
            throw e;
        }
    }
}
