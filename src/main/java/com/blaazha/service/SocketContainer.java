package com.blaazha.service;

import com.blaazha.websocket.entity.ConnectionDetails;

import javax.inject.Singleton;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

@Singleton
public class SocketContainer {
    // channel => connectiondetails
    private Map<String, Set<ConnectionDetails>> connections;

    public SocketContainer() {
        this.connections = new HashMap<>();
    }

    public Map<String, Set<ConnectionDetails>> getConnections() {
        return this.connections;
    }
}
