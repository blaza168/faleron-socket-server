package com.blaazha.service;

import java.util.Collection;

public interface SocketSender {
    void sendToRoom(String room, String header, String jsonData);

    void sendToAll(String header, String jsonData);

    void sendToUser(int id, String header, String jsonData);

    void sendToUsers(Collection ids, String header, String jsonData);
}
