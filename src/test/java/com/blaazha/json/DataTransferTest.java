package com.blaazha.json;

import com.blaazha.entity.DataTransfer;
import org.junit.Before;
import org.junit.Test;
import org.nd4j.shade.jackson.databind.ObjectMapper;

public class DataTransferTest {

    private ObjectMapper objectMapper;

    @Before
    public void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test(expected = Test.None.class) // default value. No Exception excepted
    public void TestToJsonTransfer() throws Exception {
        String body[] = new String[] { "first", "second" };
        String jsonBody = objectMapper.writeValueAsString(body);
        DataTransfer dataTransfer = new DataTransfer("header", jsonBody);

        String dataTransferJson = objectMapper.writeValueAsString(dataTransfer);
    }

    @Test
    public void showOkResponse() throws Exception {
        String respose = "OK";
        System.out.println(objectMapper.writeValueAsString(respose));
    }
}
